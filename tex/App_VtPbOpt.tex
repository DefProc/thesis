\chapter{\abbr{Viconopt} Postbuckling Optimisation}
\label{ch:App_VtPbOpt}

\abbr{Viconopt} is known to be able to optimise panels, analyse postbuckling performance and combine the two to optimise panels with both buckling and postbuckling performance as parameters.\cite{williams_user_1996} Lillico et al.\cite{lillico_post-buckling_2001} was an important source paper for this thesis. In addition to showing the results of an Abaqus testing of an infinite width, infinite length equivalent FEM panel, it also uses a panel taken from the results of a postbuckling optimisation that was performed with \abbr{viconopt}. 

Specifically, they state:
\begin{quotation}
	``\abbr{Viconopt} was used to produce a minimum mass design in which no initial buckling occured below limit load […] and only stable post-buckled behaviour occured between limit load and ultimate load.''
\end{quotation} \sourceatright{Lillico, Butler, Hunt, Watson \& Kennedy\cite{lillico_post-buckling_2001}}

As part of the research for this thesis, the author was given access to the \abbr{viconopt} data file used to perform the optimisation for the aforementioned paper. The optimisation routine both, performed as described in the paper, and produced the reported results. The input description of postbuckling has changed between earlier versions of \abbr{viconopt} used by Lillico et al. (1.33) the most recent version (1.41), so the source input file was also tested with the revised postbuckling formulation with near identical results. As such, the optimisation routine in \abbr{viconopt} works as described in the source paper.

However, while the results of the source paper are repeatable, almost every other postbuckling optimisation run that was performed in \abbr{viconopt} aborted part-way through, with little indication of the fault, or how to troubleshoot the problem. As the \abbr{viconopt} source code was not available while performing research for this thesis, it was not possible to find the cause of the failure, though it is possible to provide the details necessary to repeat the problem. 

Because the postbuckling analysis was so likely to fail in testing, it appears that \abbr{viconopt}'s optimisation routine is not yet robust enough to use with postbuckling performance as a design criteria. 

\section{Symptoms}

Running the code shown in \ref{sec:g1-pB} in Section \abbr{viconopt} 1.33 gives the error message:

\begin{alltt}
run-time-error M6201: MATH
- sqrt: DOMAIN error
\end{alltt}

and the output ``.res'' file ends with:

\begin{alltt}\scriptsize
--------------------       POST BUCKLING OUTPUT       --------------------\\
********************************************************************************\\
WARNING: TRIAL VALUE REDUCED TO   4.702665E-01 TO PREVENT \\
LONGITUDINAL LOAD IN THE PANEL EXCEEDING ITS EULER BUCKLING LOAD\\
\end{alltt}

In \abbr{viconopt} 1.41, the error message is different (this is to be expected, the postbuckling solver has changed) and the solver gets part way through the second design cycle of the optimisation before exiting with:

\begin{alltt}
run-time error M29793: MATH
- atan: error
\end{alltt}

\section{Faulty Input}
\label{sec:g1-pB}

The following \abbr{viconopt} input file listing produces the errors stated when run in \abbr{viconopt} versions 1.33 and 1.41 with the appropriate lines as indicated in the code comments.

\begin{verbatim}
PRInt 
4 5

FILe
1

TITle
Optimisation - Square Junctions 

VIPasa

BUCkling

DESign 20 20
$ ANAlysis

EIGen
1

LENgth 0.8

WAVelength		$ CHAngeset 0 = Overall MOdes +ive imperfection
1 7 1


MATerial
1 70e9 0.3 2820

LAYer
1 0.003 1 0
2 0.003 1 0
3 0.003 1 0
4 0.003 1 0
5 0.003 1 0
6 0.003 1 0
7 0.003 1 0
8 0.003 1 0
9 0.003 1 0

WALls
1 1
2 2
3 3
4 4
5 5
6 6
7 7
8 8
9 9

PLAte 
1  0.040  1
2  0.080  1
3  0.040  2
4  0.015  3
8  0.080  1
9  0.040  4
10 0.015  5
13 0.080  1
14 0.040  6
15 0.015  7
18 0.040  8
19 0.015  9
$ duplicate plates for unique plate numbers for flanges
22 0.015 3
23 0.015 5
24 0.015 7
25 0.015 9

ALIgnment
rot 5 3 270			$ Stiffener 1
off 6 5 0 -1.5e-03 0 1.5e-03
rot 7 1 180			$ other end joining plate
rot 11 9 270			$ Stiffener 2
off 12 11 0 -1.5e-3 0 1.5e-3
rot 16 14 270			$ Stiffener 3
off 17 16 0 -1.5e-3 0 1.5e-3
rot 20 18 270			$ Stiffener 4
off 21 20 0 -1.5e-3 0 1.5e-3

AXIal loading
$ Load  832000	 med
$ Load  448000	 low
Load 1337600	$ high

PREssure load
0 0 0.8e-3

connections
 1  2  1  
 2  4  6   3  4  4   4  5 22    2  6  2  	$ Stiffener 1
 6  8 12   7  8 10   8  9 23    6 10  8  	$ Stiffener 2
10 12 17  11 12 15  12 13 24   10 14 13  	$ Stiffener 3
14 16 21  15 16 19  16 17 25  			$ Stiffener 4
 1 14  7

REPetetive 
P 7

SENsitivities
B 2 3 4 8 9 10 13 14 15 18 19
T 1 2 3 4 5 6 7 8 9 

LINking
$ Constant width constraint	
2 B 1   E  -1 B 2  -1 B 8  -1 B 13  0.32
$ Non-overlapping flanges constraint
1 B 2   G   1 B 4   1 B 10
1 B 8   G   1 B 10  1 B 15
1 B 13  G   1 B 15  1 B 19
2 B 1   G   1 B 4   1 B 19
$ Web offsets equal 1/2 adjacent skin thickness
$ Top end (node 1)
1 OZ1  6 E -0.5 T 1
1 OZ1 12 E -0.5 T 1
1 OZ1 17 E -0.5 T 1
1 OZ1 21 E -0.5 T 1
$ Bottom end (node 2)
1 OZ2  6 E  0.5 T 3
1 OZ2 12 E  0.5 T 5
1 OZ2 17 E  0.5 T 7
1 OZ2 21 E  0.5 T 9
$ T-stiffeners must be symmetrical
1 B 22  E  1 B  4
1 B 23  E  1 B 10
1 B 24  E  1 B 15
1 B 25  E  1 B 19
$ Optional constraints
$ Thin-wall constraints for skin
$ 1 B 1  G  50 T 1
$ 1 B 2  G  50 T 1
$ 1 B 8  G  50 T 1
$ 1 B 13 G  50 T 1
$ Thin-wall constraints for flanges
$ 1 B 3  G  30 T 2
$ 1 B 9  G  30 T 4
$ 1 B 14 G  30 T 6
$ 1 B 18 G  30 T 8
$ Thin-wall constraints for webs
$ 1 B 4  G  10 T 3
$ 1 B 10 G  10 T 5
$ 1 B 15 G  10 T 7
$ 1 B 19 G  10 T 9

ALLowable stress
1 1 445e6 455e6 445e6 415e6 284e6

BUCkled 0.667 1.0

POStbuckling
$ SMP Method Vt.1.33
$ cycles iters imperf displ(1) displ(inc) accuracy tau plotfreq
    5     30    0.0    0.05      0.05      1.0e-4  0.3     5
$ Newton Method Vt.1.41
$ iters cycles printfreq strain(1) strain(inc) accuracy tau (not used)
$  -30    5       5        1.00      0.05        1.e-3   0.     0

  PLOt
  2 3 1 1 0
  NODes
  1 2 6 10 14
  CROss-section
  0

RESet
nacmax=70
conmin=300
NTSEG=10

CHAnge set 1		$ Overall Modes, -ive imperfection

PREssure load
0 0 -0.8e-3

END
\end{verbatim}

\section{Successful Input}
\label{sec:fmelpp1}

The complete listing of the \abbr{viconopt} input file from Lillico et al. with adjustments for postbuckling to work with \abbr{viconopt} version 1.41 is presented below for completeness.

\begin{verbatim}
$geometry
 LAYERS
     1  5.0    1  $ remote flange
     2  3.0    1  $ web 
     3  3.0    1  $ attached flange
     4  5.0    1  $ skin
     5  5.0    1  $ reduced skin
 WALL
     1  1
     2  2
     3  3  4
     4  3
     5  5
     6  4  3
 PLATES
     1  40    1    0 $ remote flange
     2  65    2    0 $ web
     3  30    6    0 $ attached flange (l)
     4  30    3    0 $ attached flange (r)
     5  90    5    0 $ unsupported skin
     6  60    4    0 $ attached flange (stiffener) for as/bt
     7  60    5    0 $ attached flange (skin) for as/bt
 ALIGNMENT
r  8  2  270
o  9  8  1.5 -2.5 0 4
r 10  3  180
o 11  4  0 0 0 1.5 
o 12 10  0 0 0 1.5
 connection
 1 2 1   2 3 9   3 5 12   3 4 11   4 5 5    
Material
1 74.99e3 0.33 2.80e-6 0 0 0 0 0 0 0    $ 0.1 per cent proof stress 466Nmm2
Allowable
1 2 0.00677 0.00677 0.00677 0.00677 0.00677 
Axial load
L 552000
print 
1 2 3 4 5
file
1
Plot
2 1 0 1 0 3
cross
0
nodes
5 3 4
Title
Postbuckling design of real panel for 42nd SDM (repetitive)
$geometry
Design 20 20
Eigenvalue
1 
Vipasa
Wavelength
2 16 1
Length 600
transverse wavelength
1 1 1              
repetitive
p 5
Sensitivities
b 1 2 3 
t 1 2 3 5
Linking
1 t 4 e 1 t 5
1 b 5 e -2 b 3 150
1 b 6 e 1 b 3
1 b 7 e 1 b 3
1 b 4 e 1 b 3
1 oy1  9 e 0.5 t 2
1 oz1  9 e -0.5 t 1  
1 oz2  9 e 0.5 t 3 0.5 t 5
1 oz2 11 e  0.5 t 3
1 oz2 12 e  0.5 t 3
$2 x 6 1 x 1 1 x 2 -1.50 x 7 -0.75 x 5 g 0
$2 x 6 1 x 1 1 x 2 -2.00 x 7 -1.00 x 5 l 0
bounds
bl 1 25 2 40 3 25 
bu 3 45
tl 1 0.5 2 0.5 3 0.5 5 0.5
press
0 0 0.6
Bukled 0.667 1.0
5
Postbuckling 
$ SMP Method (V 1.33 - original data)
$ cycles iters imperf displ(1) displ(inc) accuracy tau plotfreq
    5     30    0.0    0.05      0.05      1.0e-4  0.3     5
$ Newton Method (V 1.41)
$ iters cycles printfreq strain(1) strain(inc) accuracy tau (not used)
$  -30    5       5        1.00      0.05        1.e-3   0.     0
RESET
lxf=0 $ignore = remove
user1=0.6 $ignore
user4=1.e6 $ignore
userc=1 $ignore
userl=1 $ignore
userm=1.0 $ignore
NTSEG=10  $ignore
ndmax=1 
NUMX=25 NUMY=25 $ignore
nwid=2
mom=1
inmod=1 $ignore?
change set 1
wavelength
1 16 1
transverse wavelength
0 0 1
post
0
change set 2
wavelength
1 16 1
post 
0
transverse wavelength
1 1 1
end
\end{verbatim}

