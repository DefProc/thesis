clear all
disp ('Running...')
 
% Read Nodes.txt file and collect appropriate fields
disp ('Reading Side nodes.txt')
fid = fopen('Side nodes.txt');
C = fscanf(fid,'Coordinates of node %d :\t%e,\t %e,\t %e\n', [4,Inf]);
Nodes = C';
clear fid C
disp ('Side node.txt file read')

% Loops to check values and generate node pair matrices
EndNodes0 = zeros(1,4);
EndNodesl = zeros(1,4);
[NodesLength, null] = size(Nodes);
% Get x=min and x=max end nodes
for n = 1:NodesLength;
    if Nodes(n,2)==min(Nodes(:,2));
        EndNodes0 = [EndNodes0; Nodes(n,:)];
    elseif Nodes(n,2)==max(Nodes(:,2));
        EndNodesl = [EndNodesl; Nodes(n,:)];
    end;
end;
EndNodes0 = EndNodes0(2:end,:);
EndNodesl = EndNodesl(2:end,:);

% Sort EndNode0 Matrix and check equal nodes at each end
EndNodes0 = sortrows(EndNodes0,1);
[nEnd, null] = size(EndNodes0);
[nEndl, null] = size(EndNodesl);
 
 if nEndl ~= nEnd
    error(['End node matricies are not the same length,',...
	'you are missing some nodes data from Abaqus. ',...
	'Review the Nodes.txt file'])
else 
    clear nEndl
end;
 
 % generate node pairs matrix (EndNodes)
for n = 1:nEnd;
    for m = 1:nEnd;
        if (EndNodes0(n,4)==EndNodesl(m,4)) && (EndNodes0(n,3)==EndNodesl(m,3))
            EndNodes(n,:) = [EndNodes0(n,1),EndNodesl(m,1)];
        end
    end;
end;


clear EndNodes0 EndNodesl
[nEnd, EndNodesWidth] = size(EndNodes);
 
disp (['There are ', num2str(nEnd*EndNodesWidth), ' single nodes defined. ',...
	' These will generate ', num2str(nEnd*5), ' joining equations'])
 
 %% Data File Generation
% Initialise Keywords.txt file with instruction Header:
 
dlmwrite('keywordsside.txt',...
	 ['% This is the keywords file based on the input from "Side nodes.txt"'], '')
dlmwrite('keywordsside.txt', [' '], '-append', 'delimiter', '')
dlmwrite('keywordsside.txt', ['% Paste the lines below into the Keywords ',...
	'editor in Abaqus CAE'], '-append', 'delimiter', '')
dlmwrite('keywordsside.txt', ['% at the appropriate section - LEAVE NO BLANK LINES'],...
	'-append', 'delimiter', '')
dlmwrite('keywordsside.txt', [' '], '-append', 'delimiter', '')
dlmwrite('keywordsside.txt', ['**'], '-append', 'delimiter', '')
%Header for Dispacement Constraints
dlmwrite('keywordsside.txt',...
	 ['** Matlab Generated Contraints - Paste between "*Part" and "*End Part".'],...
	 '-append', 'delimiter', '')
dlmwrite('keywordsside.txt', ['**'], '-append', 'delimiter', '')
 
%For DOF 1-6 (not 3)
for  j=[2,3,4,5,6];
    %Constraint Title
    dlmwrite('keywordsside.txt', ['** Equal magnitude constraints (DOF', num2str(j),...
	')'], '-append', 'delimiter', '')
    dlmwrite('keywordsside.txt', ['**'], '-append', 'delimiter', '')
    %Generate Displacement Constraints
    for n=1:nEnd;
        dlmwrite('keywordsside.txt', ['*Equation'], '-append', 'delimiter', '')
        dlmwrite('keywordsside.txt', ['2'], '-append', 'delimiter', '')
        % Equal dispacement between node pairs
        dlmwrite('keywordsside.txt', [num2str(EndNodes(n,1)), ', ', num2str(j),...
		', 1., ', num2str(EndNodes(n,2)), ', ', num2str(j), ', -1.'],...
		'-append', 'delimiter', '')
        dlmwrite('keywordsside.txt', ['**'], '-append', 'delimiter', '')
    end;
end;
%Completion notice
disp ('Dispacement constraints generated')

%% Cleanup

clear EndNodesWidth NodesLength m n null
disp ('Completed')
