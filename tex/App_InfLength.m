%% Keyword Generator for the Abaqus buckling/postbuckling plate files
%--------------------------------------------------------------------
% This m-file will take a list of nodes from an Abaqus model from the 
% file Nodes.txt, determine the node pairs at each end (in the 
% z-direction) and then generate the five degree of freedom linking 
% equations for each pair. These Equations are output to the file
% Keywords.txt so they can be copied back into the Abaqus keywords
% 
% Copy the output from the message area from :
% Tools > Query > Point/Node > (Select everything) > Done
% Paste into Nodes.txt and save.
% Then run this code from the same directory.
% Copy contents of Keywords.txt into Model > Edit Keywords.
% 
% Patrick Fenner, 2010, Loughborough University
%--------------------------------------------------------------------
 
clear all
disp ('Running...')
 
% Read Nodes.txt file and collect appropriate fields
disp ('Reading Nodes.txt')
fid = fopen('Nodes.txt');
C = textscan(fid,'%s %s %s %f %f %f %f %s');
Nodes = [C{4},C{5},C{6},C{7}];
%% To run this program on GNU Octave, use the following two lines instead of the previous two:
%C = fscanf(fid,'Coordinates of node %d :	%e,	 %e,	 %e\n', [4,Inf]);
%Nodes = C';
clear fid C
disp ('Node.txt file read')
 
% Loops to check values and generate node pair matrices
EndNodes0 = zeros(1,4);
EndNodesl = zeros(1,4);
[NodesLength, null] = size(Nodes);
% Get x(z)=0 and x(z)=l end nodes
for n = 1:NodesLength;
    if Nodes(n,4)==0;
        EndNodes0 = [EndNodes0; Nodes(n,:)];
    elseif Nodes(n,4)==max(Nodes(:,4));
        EndNodesl = [EndNodesl; Nodes(n,:)];
    end;
end;
EndNodes0 = EndNodes0(2:end,:);
EndNodesl = EndNodesl(2:end,:);
 
% Sort EndNode0 Matrix and check equal nodes at each end
EndNodes0 = sortrows(EndNodes0,1);
[nEnd, null] = size(EndNodes0);
[nEndl, null] = size(EndNodesl);
 
if nEndl ~= nEnd
    error(['End node matricies are not the same length, you are missing some nodes',...
	'data from Abaqus. Review the Nodes.txt file'])
else 
    clear nEndl
end;
 
% generate node pairs matrix (EndNodes)
for n = 1:nEnd;
    for m = 1:nEnd;
        if (EndNodes0(n,2)==EndNodesl(m,2)) && (EndNodes0(n,3)==EndNodesl(m,3))
            EndNodes(n,:) = [EndNodes0(n,1),EndNodesl(m,1)];
        end
    end;
end;
 
clear EndNodes0 EndNodesl
[nEnd, EndNodesWidth] = size(EndNodes);
 
disp (['There are ', num2str(nEnd*EndNodesWidth),...
	' single nodes defined.  These will generate ', num2str(nEnd*5),...
	' joining equations'])
 
%% Data File Generation
% Initialise Keywords.txt file with instruction Header:
 
dlmwrite('keywords.txt',...
	['% This is the keywords file based on the input from "Nodes.txt"'], '')
dlmwrite('keywords.txt', [' '], '-append', 'delimiter', '')
dlmwrite('keywords.txt', ['% Paste the lines below into the Keywords editor ',...
	'in Abaqus CAE'], '-append', 'delimiter', '')
dlmwrite('keywords.txt', ['% at the appropriate section - LEAVE NO BLANK LINES'],...
	'-append', 'delimiter', '')
dlmwrite('keywords.txt', [' '], '-append', 'delimiter', '')
dlmwrite('keywords.txt', ['**'], '-append', 'delimiter', '')
%Header for Dispacement Constraints
dlmwrite('keywords.txt', ['** Matlab Generated Contraints - Paste between "*Part"',...
	' and "*End Part".'], '-append', 'delimiter', '')
dlmwrite('keywords.txt', ['**'], '-append', 'delimiter', '')
 
%For DOF 1-6 (not 3)
for  j=[1,2,4,5,6];
    %Constraint Title
    dlmwrite('keywords.txt', ['** Equal magnitude constraints (DOF', num2str(j),...
	')'], '-append', 'delimiter', '')
    dlmwrite('keywords.txt', ['**'], '-append', 'delimiter', '')
    %Generate Displacement Constraints
    for n=1:nEnd;
        dlmwrite('keywords.txt', ['*Equation'], '-append', 'delimiter', '')
        dlmwrite('keywords.txt', ['2'], '-append', 'delimiter', '')
        % Equal dispacement between node pairs
        dlmwrite('keywords.txt', [num2str(EndNodes(n,1)), ', ', num2str(j),...
		', 1., ', num2str(EndNodes(n,2)), ', ', num2str(j), ', -1.'],...
		'-append', 'delimiter', '')
        dlmwrite('keywords.txt', ['**'], '-append', 'delimiter', '')
    end;
end;
%Completion notice
disp ('Dispacement constraints generated')

%% Cleanup

clear EndNodesWidth NodesLength m n null
disp ('Completed')
