\chapter{Effect of Fillets at Plate Junctions}
\label{ch:Filleted Junctions — Extended}


The work of the previous chapter on modelling filleted junctions showed a difference in the increase of buckling load between the tested buckling mode types. It appears that the increase for local modes is related to the increased stiffness at the plate junctions that the fillets give, and the overall modes are affected more by the increase in second moment of area of the panel; however, the single set of data from the T-stiffener panel is not conclusive. 

Analysis of the effect of a blade stiffened panel would provide a more confident result though. For a blade stiffened panel, the addition of the fillets would only add mass to the stiffener skin junction, which is very close to the horizontal centroid of the panel. This means the mass addition will have a very small effect on the moment of inertia (second moment of area), compared to the increase in area. This situation is different from the T-stiffened panel, where the stiffener web-flange junction is a greater distance from the horizontal centroid, meaning a larger effect to moment of inertia than to the increase in mass.

If the hypothesis about the cause of the increase in buckling load is correct, a blade stiffened panel will show an increase in buckling load for local modes when fillets are included, with very little increase in overall mode buckling load. To better understand the relationship between fillet size and buckling load increase, testing the response of panels with a range of fillet radii should show if the buckling load increases proportionally to any of the variables.


\section{Blade Stiffened Panel}
\label{sec:Blade Stiffened Panel}


The \abbr{Nasa} Example 4 panel (also detailed in Chapter \ref{ch:Post-Buckling Modelling}) is a six blade stiffened panel that has been tested by other authors for both buckling and postbuckling performance, such as Dawe and Wang.\cite{dawe_postbuckling_2000} This panel is shown in Figure \ref{im:XSectNASAex4}, where, $l = B =0.762$m, $E =72.4$GPa and $\nu =0.32$.

\begin{figure}
  \centering
  \def\svgwidth{\columnwidth}
  \import{images/}{XSectNASA4.pdf_tex}
  \caption{Cross-Section of ``\abbr{Nasa} Example 4'' Panel. Centreline Dimensions in mm.}
  \label{im:XSectNASAex4}
\end{figure}

The \abbr{nasa} panel is modelled in \abbr{nastran}, similar to the model used in the previous chapter, but with some modifications to the length and loading method in an effort to reduce the occurrence of end-effects, which affect the accuracy of the buckling results. To show the effect of fillet size on buckling load, the radius of the fillets will be vary from $1$mm to $5$mm in respective tests. While $5$mm is a small radius for typical aeronautical panels, it was chosen as an appropriate size for the geometry of the test panel. 


\section[FEM Modelling]{ \abbr{Fem} Modelling}
\label{sec:FEM Modelling}


The \abbr{nastran} model used to calculate buckling performance of the \abbr{Nasa} Example 4 panel is very similar to the \abbr{fem} model described in Chapter \ref{ch:Filleted Junctions}. The cross section geometry is defined, and meshed with \abbr{2d} elements, and the \abbr{2d} mesh is extruded along the longitudinal axis to the panel length. This process gives a uniform mesh along the length, even where the fillets mean the cross section mesh is no longer rectangular. 

Each of the top edges of the panel skin are constrained to allow no deflection vertically (normal to the skin plane), and lateral deflection is constrained along the longitudinal mid-line of the panel as in Figure \ref{im:BCStiffPlate} on page \pageref{im:BCStiffPlate}. The axial displacement constraints however, are applied through a pair of additional nodes placed at the position of the panel centroid, one at each end of the panel. 

End-effects are the large local deflections that occur at the loaded boundaries of a model that are only the result of the analysis method, and do not occur in the modelled situation. For most \abbr{fem} models end-effects are irrelevant because the model connections represent the physical connections of a structure. In this case, it is the buckling modes for an infinite length panel that are sought, so it is important to minimise the inaccuracies caused by having loaded edges.

One particular end-effect is to stop the stiffeners from splaying at the axial ends. A \emph{rigid body element} (\abbr{rbe}) along the centre of each component stiffener, at the lateral ends, stops the stiffeners from bending; as shown in Figure \ref{im:RBEStiffPlate1}. The rigid body elements makes the specified degrees of freedom vary linearly along all the joined elements, except for the deflection along the axis of the \abbr{rbe}. This will keep the edge straight, without restricting expansion due to Poisson's effect.

\begin{figure}
	\centering
	\def\svgwidth{\columnwidth}
	\import{images/}{RBEStiffPlate1.pdf_tex}
	\caption{Rigid Body Elements Along the Plate Cross Section}
	\label{im:RBEStiffPlate1}
\end{figure}

A second set of \abbr{rbe}s then link the centroid node to every node on that edge, to keep the whole cross section planar. To avoid the possibility of introducing a stress concentration at the ends, axial displacement is applied to the centroid node, which is then distributed to all the end nodes, without it resisting rotation of the panel ends; as shown in Figure \ref{im:RBEStiffPlate2}. %Fine for Local modes, but inaccurate for overall.
This ensures that the panel end represents a simply supported condition, instead of the clamped end which would result from applying constant displacement across the cross section. 

\begin{figure}
	\centering
	\def\svgwidth{\columnwidth}
	\import{images/}{RBEStiffPlate2.pdf_tex}
	\caption{ \abbr{Rbe} Connections to the Axial Centroid Node}
	\label{im:RBEStiffPlate2}
\end{figure}

\section{Model Validation}
\label{sec:Model Validation}


To check the validity of the \abbr{fem} model, the buckling results can be compared against the same panel in \abbr{viconopt} (Table \ref{tab:FEMvsVtBuckLoad}). While the \abbr{fem} and \abbr{viconopt} results are not exactly equivalent for the filleted models (from Chapter \ref{ch:Filleted Junctions}), the results from the square junction model are equivalent.

The critical buckling displacements show a very good correlation between \abbr{fem} and \abbr{viconopt} results for $m = 1$~to~$6$, but for the $m = 1$ mode, the \abbr{fem} result has a noticeably higher buckling load. Matching the buckling displacement is not enough however, the \abbr{fem} model results must also have mode shapes equivalent to those from \abbr{viconopt}. 

%
% Table begins
% 
\begin{table}[h]
\caption{Comparison of \abbr{Fem} and \abbr{Viconopt} Buckling Loads}
\label{tab:FEMvsVtBuckLoad}
\centering
\begin{tabular}{ c c c c c }
\hline
Mode & Order of & Applied End & \multicolumn{2}{c}{Ratio to Critical Mode} \\
\cline{4-5}
($m$) & Criticality & Displacement (mm) & \abbr{Fem} & \abbr{Vipasa} \\
\hline
$6$ & $1$ & $0.3317$ & $1.0000$ & $1.0000$ \\
\hline
$5$ & $3$ & $0.3442$ & $1.0378$ & $1.0378$ \\
\hline
$4$ & $9$ & $0.3865$ & $1.1655$ & $1.1652$ \\
\hline
$3$ & $34$ & $0.4981$ & $1.5019$ & $1.5001$ \\
\hline
$2$ & $76$ & $0.8353$ & $2.5187$ & $2.4930$ \\
\hline
$1$ & $93$ & $1.0128$ & $3.0538$ & $2.8752$ \\
\hline
\end{tabular}
\end{table}

\begin{figure}
  \centering
  \def\svgwidth{0.9\columnwidth}
  \import{images/}{FillJunExtm6.pdf_tex}
  \caption{$m = 6$ Buckling Mode}
  \label{im:FillJunExtm6}
\end{figure} 

\begin{figure}
  \centering
  \def\svgwidth{0.9\columnwidth}
  \import{images/}{FillJunExtm5.pdf_tex}
  \caption{$m = 5$ Buckling Mode}
  \label{im:FillJunExtm5}
\end{figure} 

\begin{figure}
  \centering
  \def\svgwidth{0.9\columnwidth}
  \import{images/}{FillJunExtm4.pdf_tex}
  \caption{$m = 4$ Buckling Mode}
  \label{im:FillJunExtm4}
\end{figure} 

\begin{figure}
  \centering
  \def\svgwidth{0.9\columnwidth}
  \import{images/}{FillJunExtm3.pdf_tex}
  \caption{$m = 3$ Buckling Mode}
  \label{im:FillJunExtm3}
\end{figure} 

\begin{figure}
  \centering
  \def\svgwidth{0.9\columnwidth}
  \import{images/}{FillJunExtm2.pdf_tex}
  \caption{$m = 2$ Buckling Mode}
  \label{im:FillJunExtm2}
\end{figure} 

\begin{figure}
  \centering
  \def\svgwidth{0.9\columnwidth}
  \import{images/}{FillJunExtm1.pdf_tex}
  \caption{$m = 1$ Buckling Mode}
  \label{im:FillJunExtm1}
\end{figure} 

The $m=2$ to $m=6$ modes do appear to be the same as infinite length modes (see Figures \ref{im:FillJunExtm6} to \ref{im:FillJunExtm2}), though close observation does show a slight variation in the mode amplitude along the panel length. This, coupled with the \abbr{fem} to \abbr{viconopt} matched buckling displacements suggests that the boundary conditions are very close to correctly describing an infinite length case for those modes. 

A more clear discrepancy is visible in the $m=1$ mode (Figure \ref{im:FillJunExtm1}). Where the other 5 modes have the stiffener-skin junctions still axially aligned, in the overall ($m=1$) mode, the skin has moved out-of-plane, and the stiffener junctions are no longer straight. If the free edge of the stiffeners was in tension, as would be expected given the direction of bending, there should be no waviness along that edge. The wave, present in the \abbr{fem} image, suggests that there is some compression of the stiffeners that is occurring, which has a greater effect away from the longitudinal centre of the panel.

What seems to be happening in the  mode, is that the \abbr{rbe} at the panel end (visible in purple) is keeping the edge straight (as it is set to) when, for this mode, the panel boundary would not be planar in an infinite length case. The planar edge constraint is stopping each stiffener blade from rotating around the panel centroid independently of the other stiffeners. 

If the stiffeners were independent, the actual rotation of the stiffener end would be determined by the sinusoidal change in skin mode amplitude, based on the distance from the centre of the panel. This is also more likely to lead to a mode where the centre of the panel has a greater displacement than the outer bays, the opposite of which appears in Figure \ref{im:FillJunExtm1}.


\section{Results}
\label{sec:FilltExtVtResults}


Using just the critical, local, $m=6$ mode and the longest, overall, $m=1$ modes, the difference in buckling load for each mode type is shown against various radius models in Table \ref{tab:VarBuckLoadm=6} and Table \ref{tab:VarBuckLoadm=1}. The proportional increase in the local buckling load is clearly much larger than the increase of the overall buckling load. 

Similarly, from Table \ref{tab:VarMassAndIntertia}, the increase in cross section area is much greater than the increase in moment of inertia for each buckling load. Because of the range of fillet radii tested, it is very likely that the local mode increase is linked to the increase in stiffness around the stiffener-skin junctions, and the small overall mode increase is due to the small increase in the panel's bending stiffness. 

%
% Table begins
% 
\begin{table}[h]
\caption{Variation of Buckling Load for Local, $m=6$, Mode.}
\label{tab:VarBuckLoadm=6}
\centering
\begin{tabular}{ c c c c c }
\hline
Fillet Radius & Order of & Applied End & \multicolumn{2}{c}{\multirow{2}{*}{Buckling Load (N)}} \\
(mm) & Criticality & Displacement (mm) & & \\
\hline
$0$ & $1$ & $0.33163$ & $39322.93$ & - \\
\hline
$1$ & $1$ & $0.33734$ & $40648.12$ & $+3.370\%$ \\
\hline
$2$ & $1$ & $0.34795$ & $42181.98$ & $+7.271\%$ \\
\hline
$3$ & $1$ & $0.36517$ & $44716.27$ & $+13.716\%$ \\
\hline
$4$ & $1$ & $0.39066$ & $48506.65$ & $+23.355\%$ \\
\hline
$5$ & $1$ & $0.42325$ & $53485.48$ & $+36.016\%$ \\
\hline
\end{tabular}
\end{table}

%
% Table begins
% 
\begin{table}[h]
\caption{Variation of Buckling Load for Overall, $m=1$, mode}
\label{tab:VarBuckLoadm=1}
\centering
\begin{tabular}{ c c c c c }
\hline
Fillet Radius & Order of & Applied End & \multicolumn{2}{c}{\multirow{2}{*}{Buckling Load (N)}} \\
(mm) & Criticality & Displacement (mm) & & \\
\hline
$0$ & $95$ & $1.0145$ & $120294.03$ & - \\
\hline
$1$ & $86$ & $1.0016$ & $120688.79$ & $+0.328\%$ \\
\hline
$2$ & $85$ & $1.0003$ & $121266.37$ & $+0.808\%$ \\
\hline
$3$ & $85$ & $0.99644$ & $122017.37$ & $+1.433\%$ \\
\hline
$4$ & $80$ & $0.98456$ & $122248.78$ & $+1.625\%$ \\
\hline
$5$ & $78$ & $0.97375$ & $123051.36$ & $+2.292\%$ \\
\hline
\end{tabular}
\end{table}
%
% Table begins
% 
\begin{table}[h]
\caption{Variation of Mass and Moment of Inertia with Fillet Junctions}
\label{tab:VarMassAndIntertia}
\centering
\begin{tabular}{ c c c c c }
\hline
Fillet Radius & \multicolumn{2}{c}{Second Moment of Area} & \multicolumn{2}{c}{Cross Sectional Area} \\
(mm) & (mm$^4$) & & (mm$^2$) & \\
$0$ & $97986.34$ & - & $1265.625$ & - \\
\hline
$1$ & $98013.56$ & $+0.028\%$ & $1268.2$ & $+0.203\%$ \\
\hline
$2$ & $98079.77$ & $+0.095\%$ & $1275.926$ & $+0.814\%$ \\
\hline
$3$ & $98163.54$ & $+0.181\%$ & $1288.802$ & $+1.831\%$ \\
\hline
$4$ & $98247.01$ & $+0.266\%$ & $1306.828$ & $+3.256\%$ \\
\hline
$5$ & $98316.89$ & $+0.337\%$ & $1330.005$ & $+5.087\%$ \\
\hline
\end{tabular}
\end{table}


From Figure \ref{im:BuckLoadXSectArea}, the buckling load for the $m=6$ (local buckling) mode rises almost proportionally to the cross sectional area of the panel, though the gradient of the graph increases slightly from $1$–$5$mm radius. 

\begin{figure}
	\centering
	\def\svgwidth{\columnwidth}
	\import{images/}{BuckLoadXSectArea.pdf_tex}
	\caption{Buckling Load for $m=6$ Mode, Compared to Cross-Sectional Area}
	\label{im:BuckLoadXSectArea}
\end{figure}

The buckling load for any plate with clamped longitudinal edges is 1.75 times the buckling load for the same mode shape for a panel with simply supported edges all round.\cite{iyengar_structural_1988} As such, if the increase in buckling load for a local mode for this panel was purely due to the increase in rotational stiffeners at the panel junctions, the buckling load for the $m=6$ mode for this panel might be expected to rise asymptotically towards $175\%$ of the square junction result, as the panel fillet radius increases.

However, the additional area of the fillets also reduces the effective width of the component plates in the panel. Therefore an increase in buckling load for the panel proportional to $b^2$ might also be expected — also meaning that the buckling load cannot be limited to 1.75 times the square junction result. The separation of area and stiffness could be investigated by modelling the increase in stiffness at the plate junctions as rotational stiffness in \abbr{vicon} or \abbr{fem}.

\begin{figure}
	\centering
	\def\svgwidth{\columnwidth}
	\import{images/}{BuckLoadMomtInertia.pdf_tex}
	\caption{Buckling Load for $m=1$ Mode, Compared to Moment of Inertia}
	\label{im:BuckLoadMomtInertia}
\end{figure}

Correlation is less clear between the buckling load and the moment of inertia for the $m=1$ mode (Figure \ref{im:BuckLoadMomtInertia}), though, this may be from two sources.  It could be that the buckling mode of the model does not closely enough represent the infinite length condition, so the inaccurate buckling mode shape, as shown in Figure \ref{im:FillJunExtm1} is affecting the results as discussed above.  Or, because the buckling mode is not true Euler buckling, as the longitudinal edges are supported, the increase in buckling mode may not be linearly related to moment of inertia because the centre of the panel bends more than the supported edges, meaning the increased moment of inertia has more effect in centre of the panel.
%this needs bettermenting!

\section{Conclusions}
\label{sec:Conclusions}


The blade stiffened panel results show that the increase in local mode stiffness is controlled by the additional stiffness caused by the filleted junction, while the buckling load of the overall mode is increased due to the increased bending stiffness of the panel.

For local modes, where the stiffener junction line remains straight after buckling, the extra volume of the junction fillet increases the rotational stiffness of the joint line. As cross-sectional area increases, the junction acts less like a simple line support, and tends more towards a fixed boundary. While this suggests that the upper limit of the buckling load increase is $75\%$\cite{iyengar_structural_1988} --- the maximum increase in plate buckling load between simply supported edges and fixed edges --- for extremely large fillet radii, the increase is probably greater, as the additional area also reduces the ``unsupported'' width of the skin-plate.

The much lesser increases of the overall mode indicate that the buckling load is more influenced by the increase in bending stiffness of the panel. It is also likely, because a supported plate bends less at the edges in an overall mode, that the additional mass will have a greater effect in the mid-line of the panel, where the modal curvature of the junction lines is greatest.

Even if the existence of fillets at the junctions is not included in any panel design process, care must be taken to ensure that the additional buckling load, which has a much greater effect on the local buckling modes, does not cause the practical panel to have coincident local and overall buckling loads.


\section{Further Work}
\label{sec:Further Work}


It is clear from the overall buckling mode shape, that the current model still does not exactly represent the same infinite length panel that is assumed in \abbr{viconopt}. The use of rigid body elements at the loaded ends is not able to omit all of the end-effects, so alternative boundary conditions could be investigated to improve the accuracy of these results. Better boundary conditions are especially important if the \abbr{fem} modelling of infinite length plates is extended into the postbuckling region, as it is the mode shape behaviour that dictates postbuckling performance. 


\section{Publications}
\label{sec:Publications}


A paper based on the work in this chapter was published in the Thin-Walled Structures Journal: Fenner PE, Watson A, Finite Element Buckling Analysis of Stiffened Plates with Filleted Junctions. \emph{Thin Walled Structures}, 59, 2012, pp.\ 171-180.\cite{fenner_finite_2012}


